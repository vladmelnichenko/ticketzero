<?php
require_once('libs/template.class.php');
require_once('server/config/safemysql.class.php');
require_once('server/config/security.php');

session_start();


if(isset($_POST['email']) && isset($_POST['password'])) {

$db=new SafeMySQL();
$data = array('login' => $_POST['email'], 'password' => $_POST['password']);
$sql  = "INSERT INTO users  (user_email,user_password,user_salt) VALUES(?s,?s,?s)";
$salt=salt();
$password=crypt($_POST['password'], $salt);
$db->query($sql,$_POST['email'],$password,$salt);
	 $_SESSION['user_email']=$_POST['email'];
	 $_SESSION['user_name']='';
	 $_SESSION['user_id']=$db->insertId();
	  header("Location:dashboard");
} else {

$tmpl = new Template('signup');
echo $tmpl->getText();
}
?>