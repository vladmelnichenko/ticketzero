<?php
session_start();

require_once('../server/config/safemysql.class.php');
require_once('../libs/ajax-lng/get-lng.php');


$db = new SafeMySQL();
/*
$countAll = $db->getOne("
SELECT 
  COUNT(*) 
FROM 
  tickets t,
  group_users ga,
  group_queue gq,
  users a
WHERE t.queue_id=gq.queue_id
  AND gq.group_id=gq.group_id
  AND ga.user_id=a.user_id
  AND t.ticket_status_id='new'
  AND a.user_id=?i"
, $_SESSION['user_id']);

$sCountAll = "";
if ((int) $countAll > 0)
    $sCountAll = "(" . $countAll . ")";

$countMy = $db->getOne("
SELECT 
   COUNT(*) 
FROM   
   tickets t
WHERE t.ticket_status_id='inprogress'
  AND t.user_id=?i"
, $_SESSION['user_id']);

$sCountMy = "";
if ((int) $countMy > 0)
    $sCountMy = "(" . $countMy . ")";

*/


$queue = $db->getAll("
SELECT
  q.queue_id,
  q.queue_name,
  COUNT(t.ticket_id) AS ticket_count
FROM (SELECT
  q.queue_id,
  q.queue_name,
  q.queue_sortindex
FROM queue q,
     group_queue gq,
     group_users ga
WHERE q.queue_id = gq.queue_id
AND ga.group_id = gq.group_id
AND ga.user_id = ?i) AS q
  LEFT JOIN tickets t
    ON q.queue_id = t.queue_id
    AND t.ticket_status_id = 'new'
GROUP BY q.queue_id,
         q.queue_name
ORDER BY q.queue_sortindex"
, $_SESSION['user_id']);

$sQueueList = "";
foreach ($queue as $value) {
    if ($sQueueList !== "")
        $sQueueList.=",";

    //$sTicketCount = $value['ticket_count'] > 0 ? " (" . $value['ticket_count'] . ")" : "";
    $sQueueList.="{id: \"" . $value['queue_id'] . "\", value: \"" . $value['queue_name'] .  "\", icon: \"table\", details: \"".$value['queue_name']."\", queue_type: \"queue\", ticket_count: 0, ticket_unread_count: 0}";
}

$agents = $db->getAll("
SELECT
  usr.user_id,
  usr.user_name,
  COUNT(t.ticket_id) as ticket_count
FROM (SELECT
  u.user_id,
  CONCAT(u.user_secondname, ' ', u.user_firstname) AS user_name
FROM users u,
     workspace_users wu
WHERE u.user_id = wu.user_id
AND u.role_id IN (1, 2)
AND u.user_id<>?i
AND wu.workspace_id = ?i) AS usr
  LEFT JOIN tickets t
    ON usr.user_id = t.user_id
GROUP BY usr.user_id,
         usr.user_name
ORDER BY usr.user_name"
, $_SESSION['user_id'], $_SESSION['workspace_id']);

$sAgentList = "";
foreach ($agents as $value) {
    if ($sAgentList !== "")
        $sAgentList.=",";

    //$sTicketCount = $value['ticket_count'] > 0 ? " (" . $value['ticket_count'] . ")" : "";
    $sAgentList.="{id: \"" . $value['user_id'] . "\", value: \"" . $value['user_name'] . "\", icon: \"user\", details: \"".$value['user_name']."\", queue_type: \"user\" , ticket_count: 0, ticket_unread_count: 0}";
}



header('Content-Type: application/json');
echo "[
                        {id: \"main\", value: \"".tr('sb_main')."\", open: true, data: [
                                {id: \"-100\", value: \"".tr('sb_all_tickets')."\", badge:12, icon: \"tasks\", \$css: \"dashboard\", details: \"Все заявки\", queue_type : \"all\" , ticket_count: 0, ticket_unread_count: 0},
                                {id: \"-200\", value: \"".tr('sb_assign_to_me')."\", icon: \"user\", \$css: \"orders\", details: \"Назначено мне\" , queue_type : \"my\" , ticket_count: 0, ticket_unread_count: 0},
                                {id: \"-300\", value: \"".tr('sb_int_progress')."\", icon: \"lock\", \$css: \"orders\", details: \"В работе\" , queue_type : \"inprogress\" , ticket_count: 0, ticket_unread_count: 0},
                                {id: \"-400\", value: \"".tr('sb_trash')."\", icon: \"trash\", \$css: \"orders\", details: \"Корзина\", queue_type : \"trash\" , ticket_count: 0, ticket_unread_count: 0}
                                ]
                        },
                        {id: \"main1\", value: \"".tr('sb_queue')."\", open: true, data: [
                        " . $sQueueList . "
                            ]},
                        {id: \"main2\", value: \"".tr('sb_agents')."\", open: true, data: [
                        " . $sAgentList . "
                            ]},    
                        {id: \"main31\", value: \"".tr('sb_statistics')."\", open: true, data: [
                                {id: \"-1000\", value: \"".tr('sb_archive')."\", icon: \"archive\", \$css: \"orders\", details: \"Архив\", queue_type : \"archive\" , ticket_count: 0, ticket_unread_count: 0},
                                {id: \"-1001\", value: \"".tr('sb_reports')."\", icon: \"archive\", \$css: \"orders\", details: \"Отчеты\", queue_type : \"reports\" , ticket_count: 0, ticket_unread_count: 0},
                        ]},    
                        {id: \"main3\", value: \"".tr('sb_admin')."\", open: true, data: [
                                {id: \"-2000\", value: \"".tr('sb_queue')."\", icon: \"tasks\", \$css: \"dashboard\", details: \"Очереди\", queue_type : \"admin\" , ticket_count: 0, ticket_unread_count: 0},
                        ]}    
                        ]"
?>