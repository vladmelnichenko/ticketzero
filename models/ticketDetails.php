<?php

require_once('../server/config/safemysql.class.php');

$db = new SafeMySQL();
$data = $db->getAll("  
SELECT DISTINCT
tm.user_id,
tm.ticket_message_date, 
tm.ticket_message_text ,
IF(tm.user_id=0,CONCAT(COALESCE(ticket_message_sender_name,''),' [',ticket_message_sender_email,']'),CONCAT(u.user_secondname,' ',u.user_firstname)) AS user_name,
tm.ticket_message_attachments
FROM ticket_messages tm,
 users u
WHERE  ((tm.user_id=u.user_id) OR (tm.user_id=0))
  AND tm.ticket_id = ?i
ORDER BY ticket_message_date DESC",
$_GET['id']);




header('Content-Type: application/json');
echo json_encode($data);
?>