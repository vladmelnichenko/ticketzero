<?php
require_once('../server/config/safemysql.class.php');

$db = new SafeMySQL();

$data = $db->getAll("
SELECT a.autoanswer_id as id, a.autoanswer_name as value
  FROM autoanswer a,
       queue_autoanswer qa
  WHERE a.autoanswer_id=qa.autoanswer_id
    AND a.autoanswer_type=?s
   UNION ALL 
SELECT a.autoanswer_id as id, a.autoanswer_name as value
  FROM autoanswer a
  WHERE a.autoanswer_general=1
    AND a.autoanswer_type=?s
", $_GET['target'], $_GET['target']);

header('Content-Type: application/json');
echo json_encode($data);

?>