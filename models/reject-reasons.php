<?php
require_once('../server/config/safemysql.class.php');

$db = new SafeMySQL();

$data = $db->getAll("
SELECT reject_reason_id as id, reject_reason_name as value
  FROM reject_reasons
");

header('Content-Type: application/json');
echo json_encode($data);

?>