<?php

require_once('../server/config/safemysql.class.php');


session_start();


$filterStr = " WHERE 1=1 ";


if (isset($_GET['last_update']))
    $filterStr = $filterStr . " AND ticket_datecreate> '" . $_GET['last_update'] . "' ";

$searchClosed = false;

if (isset($_GET['filter'])) {
    $filter = array();
    $filter = json_decode($_GET['filter'],true);
    foreach ($filter as $key => $value) {
        if ($key == 'search_closed') {
            if ($value == '1') {
                $searchClosed = true;
                continue;
            }
        }

         $aParams = split(":",$value);

        if ($key == 'ticket_number') {
            $filterStr .= " AND " . $key . " LIKE '%" . $aParams[1] . "' " ;
            continue;
        }



        if ($aParams[0] == 'string') {
            $filterStr .= " AND " . $key . " LIKE '%" . $aParams[1] . "%' " ;
        }

        if ($aParams[0] == 'int') {
            $filterStr .= " AND " . $key . " = " . $aParams[1];
        }

    }
}



if ($_GET['queue_type'] == "archive")
    $filterStr.=" AND qry.ticket_status_id='closed' ";
else
if ($_GET['queue_type'] == "trash")
    $filterStr.=" AND qry.ticket_status_id='deleted' ";
else
if (!$searchClosed)
    $filterStr.=" AND qry.ticket_status_id!='closed' ";



$db = new SafeMySQL();
if (($_GET['queue_type'] == "all") || ($_GET['queue_type'] == "archive") || ($_GET['queue_type'] == "trash")) {



    $data = $db->getAll("  SELECT * FROM (SELECT tik.*,IFNULL(CONCAT(usr.user_secondname,' ',SUBSTRING(usr.user_firstname,1,1)),usr.user_email) AS ticket_user, COALESCE(tu.ticket_id,0) AS ticket_unread   FROM (
 SELECT 
                      ticket_id as id,  
                      ticket_id, 
                      ticket_number, 
                      CONCAT('',ticket_subject,'') as ticket_subject, 
                      t.ticket_datecreate as  ticket_age,
                      q.queue_name as ticket_queue,
                      t.ticket_sender_email,
                      t.ticket_sender_name,
                      t.ticket_datecreate,
                      t.queue_id,
                      t.user_id,
                      ts.ticket_status_name,
                      ts.ticket_status_id
                      FROM 
                      tickets t, 
                      queue q,
                      group_users ga,
                      group_queue gq,
                      users u,
                      ticket_status ts
                      WHERE t.queue_id=q.queue_id
                      AND gq.queue_id=q.queue_id
                      AND gq.group_id=gq.group_id
                      AND ga.user_id=u.user_id
                      AND u.user_id=?i
                      AND t.ticket_status_id=ts.ticket_status_id
  ) AS tik LEFT JOIN users usr 
  ON tik.user_id=usr.user_id
LEFT JOIN ticket_unread tu ON tu.user_id=?i AND tu.ticket_id=tik.ticket_id
) AS qry
  " . $filterStr . "
 ORDER BY qry.ticket_age DESC", $_SESSION['user_id'] , $_SESSION['user_id']);
}


if (($_GET['queue_type'] == "my") || ($_GET['queue_type'] == "user")) {
    if ($_GET['queue_type'] == "my")
        $userId = $_SESSION['user_id'];
    else
        $userId = $_GET['id'];

    $data = $db->getAll("  
                      SELECT 
                      ticket_id as id,  
                      ticket_id, 
                      ticket_number, 
                      CONCAT('',ticket_subject,'') as ticket_subject, 
                      t.ticket_datecreate as  ticket_age,
                      q.queue_name as ticket_queue,
                      t.ticket_sender_email,
                      t.ticket_sender_name,
                      t.queue_id,
                      t.user_id,
                      ts.ticket_status_name,
                      IFNULL(CONCAT(u.user_secondname,' ',SUBSTRING(u.user_firstname,1,1)),u.user_email) AS ticket_user,
                      ts.ticket_status_id,
                      0 ticket_unread
                      FROM 
                      tickets t, 
                      queue q,
                      users u,
                      ticket_status ts
                      WHERE t.queue_id=q.queue_id
                      AND t.user_id=?i
                      AND t.user_id=u.user_id
                      AND t.ticket_status_id<>'deleted'
                      AND t.ticket_status_id<>'closed'
                      AND t.ticket_status_id=ts.ticket_status_id
 ORDER BY t.ticket_datecreate", $userId);
}


if (($_GET['queue_type'] == "inprogress")) {

    $data = $db->getAll("  
                      SELECT 
                      ticket_id as id,  
                      ticket_id, 
                      ticket_number, 
                      CONCAT('',ticket_subject,'') as ticket_subject, 
                      t.ticket_datecreate as  ticket_age,
                      q.queue_name as ticket_queue,
                      t.ticket_sender_email,
                      t.ticket_sender_name,
                      t.queue_id,
                      t.user_id,
                      ts.ticket_status_name,
                      IFNULL(CONCAT(u.user_secondname,' ',SUBSTRING(u.user_firstname,1,1)),u.user_email) AS ticket_user,
                      ts.ticket_status_id,
                      0 ticket_unread
                      FROM 
                      tickets t, 
                      queue q,
                      users u,
                      ticket_status ts
                      WHERE t.queue_id=q.queue_id
                      AND t.user_id=?i
                      AND t.ticket_status_id='inprogress'
                      AND t.user_id=u.user_id
                      AND t.ticket_status_id=ts.ticket_status_id
 ORDER BY t.ticket_datecreate", $_SESSION['user_id']);
}



if ($_GET['queue_type'] == "queue") {
    $data = $db->getAll("SELECT * FROM (SELECT tik.*, COALESCE(tu.ticket_id,0) AS ticket_unread   
        FROM (
        SELECT
                      ticket_id as id,  
                      ticket_id, 
                      ticket_number, 
                      CONCAT('',ticket_subject,'') as ticket_subject, 
                      t.ticket_datecreate as  ticket_age,
                      q.queue_name as ticket_queue,
                      t.ticket_sender_email,
                      t.ticket_sender_name,
                      t.queue_id,
                      t.user_id,
                      ts.ticket_status_name,
                      ts.ticket_status_id,
                      t.ticket_datecreate
                      FROM 
                      tickets t, 
                      queue q,
                      ticket_status ts
                      WHERE t.queue_id=q.queue_id
                      AND t.queue_id=?i
                      AND t.user_id =0
                      AND t.ticket_status_id<>'deleted'  AND t.ticket_status_id<>'closed'
                      AND t.ticket_status_id=ts.ticket_status_id
) as tik 
LEFT JOIN ticket_unread tu ON tu.user_id=1 AND tu.ticket_id=tik.ticket_id                          
) AS qry "
            . $filterStr .
            " ORDER BY ticket_datecreate                       
 ", $_GET['id']);
}


header('Content-Type: application/json');


for ($i = count($data) - 1; $i >= 0; $i--) {
    $interval = date_diff(new DateTime($data[$i]['ticket_age']), new DateTime());
    $data[$i]['ticket_age'] = $interval->format("%dд %Hч");
    if ($data[$i]['ticket_unread'] == "0")
        $data[$i]['$css'] = "ticket_row_normal";
    else
        $data[$i]['$css'] = "ticket_row_new";
}

echo json_encode($data);
?>
