define(["views/ticketList",
    "views/ticketDetail"
], function (ticketList, ticketDetail) {

    return {
        $ui: {
            id: "ticket-area",
            //header: "List",
            body: {
                cells: [{
                    view: "accordion",
                    id: "area-list",
                    rows: [
                        {
                            id: "tabview-tickets",
                            body: {rows: [ticketList.$ui]}
                        },
                        {
                            view: "resizer",
                            id: "tabview-resizer",
                            on: {
                                onItemClick: function (id, e, node) {
                                    aler('ddd');
                                }
                            },
                        },
                        {body: ticketDetail.$ui}
                    ]


                },
                    {
                        id: "formView",
                        view: "form",
                        scroll: false,
                        elements: [
                            {view: "text", label: "Rank", name: "rank", labelWidth: 70},
                            {view: "text", label: "Title", name: "title", placeholder: "Book title", labelWidth: 70},
                            {view: "text", label: "Year", name: "year", placeholder: "Year", labelWidth: 70},
                            {
                                margin: 5, cols: [
                                {view: "button", value: "Cancel", click: "cancel()"},
                                {view: "button", value: "Save", type: "form", click: "save()"}
                            ]
                            },
                            {}
                        ]
                    }

                ]
            }

        }
    }


});