define(function () {


    return {
        $ui: {
            view: "toolbar",
            id: "toolbar",
            css: "header",
            height: 32, paddingY: 0, elements: [
                {
                    view: "icon",
                    icon: "navicon",
                    width: 32,
                    popup: "main_menu"
                },
                {},
                {
                    height: 32,
                    id: "person_template",
                    css: "header_person",
                    borderless: !0,
                    template: function (e) {
                        var t = "<div style='height:100%;width: auto; float:right; ' onclick='webix.$$(\"profilePopup\").show(this)'>";
                        return t += "<img id=\"toolbar-avatar\"  class='photo' src='server/get-avatar.php?user_id=" + gUserId + "' /><span class='name'>" + gUserName + "</span>", t += "<span class='webix_icon fa-angle-down'></span></div><div style='height:32px;width: auto; float:right; ' onclick='webix.$$(\"searchPopup\").show(this)'><button type=\"button\" style=\"height:32px;width:100%;\" class=\"webix_icon_button\"><span class=\"webix_icon fa-search \"></span></button></div> \n\
<div style='height:32px;width: auto; float:right; ' onclick='webix.$$(\"searchPopup\").show(this)'><button type=\"button\" style=\"height:32px;width:100%;\" class=\"webix_icon_button\"><span class=\"webix_icon fa-filter \"></span></button></div>  "
                    }
                }


            ],
            data: {
                id: 26,
                name: "sss"
            }
        }
    }


});
