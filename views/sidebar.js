define(function () {

    webix.ui({
        view: "contextmenu",
        id: "sidebar-context",
        data: [{id: 1, value: "Пометить все как прочитанные"}],
        width: 300
    });


    return {
        $ui: {
            rows: [
                {
                    view: "tree",
                    id: "sidebar",
                  //  type: "menuTree2",
                    css: "menu",
                    //width: 200,
                    autoWidth: true,
                    drag: true,
                    activeTitle: true,
                    navigation: true,
                    select: true,

                    scroll: "y",
                    on: {
                        onViewResize: function () {
                            alert(this.config.id);
                        }
                    },
                    tooltip: {
                        template: function (obj) {
                            return obj.$count ? "" : obj.details;
                        }
                    },
                    url: "models/sidebar.php",
                    type: {
                        template: function (obj, common) {
                            html = "";
                            sTicketCount = '';
                            if (obj.ticket_count > 0) {
                                sTicketCount = ' (' + obj.ticket_count + ')';
                            }
                            if (obj.icon)
                                html = "<span class='webix_icon icon fa-" + obj.icon + "'></span>";

                            if (obj.$level == 1) {
                                var dir = obj.open ? "down" : "right";
                                html = "<span class='" + open + " webix_icon fa-angle-" + dir + "'></span>";

                            }
                            if (obj.ticket_unread_count > 0) {
                                return '<div style="float:left; display: inline;">' + html + obj.value + sTicketCount + '</div><div style="float:right; display: inline; padding-top: 8px; "><div class="sidebar_badge">' + obj.ticket_unread_count + '</div></div>';
                            } else
                                return '<div style="float:left; display: inline;">' + html + obj.value + sTicketCount + '</div>';

                        }
                    },
                }
            ]
        }
    }
});

