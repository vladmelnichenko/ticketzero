define(["views/menus/autoanswerReply",
    "views/menus/autoanswerClose"], function (autoanswerReply,
                                              autoanswerClose) {


    return {
        $ui: {
            rows: [
                {
                    rows: [{
                        view: "form",
                        id: "ticket-form-reply",
                        hidden: true,
                        elements: [
                            {
                                view: "layout", id: "lt_close_reason", hidden: true, cols: [{
                                view: "combo", width: 200,
                                id: "cb-form-close-type",
                                label: 'Тип', labelWidth: 40, labelAlign: "right", name: "cb-form-close-type",
                                //  hidden:true,
                                value: 1, yCount: "3", options: [
                                    {id: 1, value: "Успешно"},
                                    {id: 2, value: "Не успешно"}
                                ]
                            },
                                {
                                    view: "combo",
                                    id: "cb-form-close-reason",
                                    //  hidden:true,
                                    label: 'Причина',
                                    labelWidth: 100,
                                    labelAlign: "right",
                                    name: "cb-form-close-reason",
                                    value: 0,
                                    yCount: "3",
                                    options: "models/reject-reasons.php"
                                }
                            ]
                            },
                            {
                                view: "layout", id: "lt_message_type", hidden: true, cols: [{
                                view: "combo", width: 360,
                                id: "cb-form-reply-type",
                                label: 'Тип', labelWidth: 40, labelAlign: "right", name: "cb-form-reply-type",
                                //  hidden:true,
                                value: 1, yCount: "2", options: [
                                    {id: 1, value: "Ответ пользователю"},
                                    {id: 2, value: "Внутренняя заметка"}
                                ]
                            }, {}
                            ]
                            },
                            {
                                view: "template",
                                //template: "<div style=\"background-color:red;\"><textarea name=\"editor-msg\" id=\"editor-msg\" style=\"width:200px; height:132px;\"></textarea></div>",
                                template: "<textarea name=\"editor-msg\" id=\"editor-msg\" ></textarea>",
                                type: "clean",
                                width: "auto",
                                height: 320
                            },
                            {
                                margin: 5, width: "auto", cols: [
                                {
                                    width: 500, cols: [{
                                    view: "button",
                                    id: "btn-reply-form-reply",
                                    value: "Отправить",
                                    css: "button_success button_raised",
                                    click: function () {
                                    }
                                },
                                    {
                                        view: "button",
                                        id: "btn-reply-form-close",
                                        value: "Отмена",
                                        css: "button_primary button_raised"
                                    }, {rows: [{}]}
                                ]
                                }
                            ]
                            }


                        ]
                    }]
                },
                {
                    view: "list",
                    collapsed: false,
                    //height:600,
                    id: "ticket-detail",
                    select: false,
                    type: {
                        templateStart: function (e) {
                            if (e.user_id == gUserId)
                                sClass = 'agent';
                            else
                                sClass = 'user';
                            return "<div style='clear :both; padding-top: 20px; '><div class='bubble_user u" + sClass + "'>" + e.user_name + "<br><span class='bubble_time'>" + e.ticket_message_date + "</span></div><img class='bubble_avatar a" + sClass + "'   src='server/get-avatar.php?user_id=" + e.user_id + "'><div item_id='id' class='bubble b" + sClass + "'>"
                        },
                        template: "#ticket_message_text#<br>#ticket_message_attachments#<br><div style='text-align:right;'></div>",
                        templateEnd: "</div></div>",
                    }

                }
            ]
        }

    }


});