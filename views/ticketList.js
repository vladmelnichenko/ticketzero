define(["views/menus/autoanswerReply",
    "views/menus/autoanswerClose"], function (autoanswerReply, autoanswerClose) {


    return {
        $ui: {
            rows: [{
                view: "toolbar", paddingY: 2,
                cols: [
                    {
                        view: "button",
                        id: "btn-ticket-reply",
                        label: "Ответ",
                        type: "iconButton",
                        icon: "reply",
                        width: 120
                    },
                    {
                        view: "button",
                        id: "btn-autoanswer-reply",
                        type: "iconButton",
                        icon: "angle-down",
                        width: 44,
                        popup: autoanswerReply.$ui
                    },
                    {
                        view: "button",
                        id: "btn-ticket-block",
                        type: "iconButton",
                        icon: "lock",
                        label: "Блокировать",
                        height: 32,
                        width: 200
                    },
                    {
                        view: "button",
                        id: "btn-ticket-close",
                        type: "iconButton",
                        icon: "check",
                        label: "Закрыть",
                        height: 32,
                        width: 140
                    },
                    {
                        view: "button",
                        id: "btn-autoanswer-close",
                        type: "iconButton",
                        icon: "angle-down",
                        width: 44,
                        popup: autoanswerClose.$ui
                    },
                    {
                        view: "button",
                        id: "btn-ticket-comment",
                        type: "iconButton",
                        icon: "comment-o",
                        height: 32,
                        width: 44
                    }
                ]
            }, {
                collapsed: true,
                id: "ticket-list",
                view: "datatable",
                drag: true,
                multiselect: true,
                //rowHeight:52,
                select: "row",
                navigation: true,
                resizeColumn: true,
                columns: [
                    {id: "id", hidden: "true"},
                    {
                        id: "ticket_id",
                        icon: "tasks",
                        header: "Номер",
                        css: "rank",
                        width: 100,
                        hidden: "true",
                        sort: "string"
                    },
                    {id: "ticket_number", header: "Номер", css: "rank", width: 100, sort: "string"},
                    {
                        id: "ticket_status_id",
                        header: "1",
                        css: "rank",
                        width: 100,
                        template: "<span  style='padding-top:12px;' class='webix_icon fa-bug'></span>",
                        sort: "string"
                    },
                    //{id: "2q", header: "1", css: "rank", width: 34, template: "<span  style='color:#F69D04; padding-top:12px; cursor:pointer;' class='webix_icon fa-comment'></span>" },
                    {
                        id: "ticket_subject", header: "Тема", width: 200, fillspace: true,
                        // template : "<img src=\"resources/images/bug.png\"><span  style='color:#777777; cursor:pointer;' class='ticket_subject'><strong>#ticket_subject#</strong></span><span class='ticket_second'>#ticket_sender#</span>"
                        //template : "<strong>#ticket_subject#</strong>"
                    },
                    {id: "ticket_queue", header: "Очередь", width: 120, sort: "string"},
                    {id: "ticket_status_name", header: "Статус", width: 100, sort: "string"},
                    {id: "ticket_user", header: "Агент", width: 130, sort: "string"},
                    {id: "ticket_age", header: "Возраст", width: 100, sort: "string"},
                    {id: "ticket_sender", hidden: "true"},
                    {id: "queue_id", hidden: "true"},
                    {id: "ticket_status_id", hidden: "true"},
                    {id: "quene_type", hidden: "true"},
                    {id: "ticket_unread", hidden: "true"}
                ]
            }
            ]
        }
    }
});