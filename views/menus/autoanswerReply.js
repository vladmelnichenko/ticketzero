define(["controllers/ticketDetailController"], function (ticketDetailController) {

    return {
        $ui: {
            view: "submenu",
            id: "autoanswer-reply",
            width: 300,
            padding: 0,
            url: "models/autoanswer.php?target=reply&queue_id=",
            on: {
                onItemClick: function (id, e, node) {
                    $$('autoanswer-reply').hide();
                    ticketDetailController.autoAnswer('reply', id);
                }
            }
        }
    };
});