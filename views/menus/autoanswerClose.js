define(["controllers/ticketDetailController"], function (ticketDetailController) {

    return {
        $ui: {
            view: "submenu",
            id: "autoanswer-close",
            width: 300,
            padding: 0,
            url: "models/autoanswer.php?target=close&queue_id=",
            on: {
                onItemClick: function (id, e, node) {
                    $$('autoanswer-close').hide();
                    ticketDetailController.autoAnswer('close', id);
                }
            }
        }
    };
});