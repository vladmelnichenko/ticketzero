define(["views/sidebar", "controllers/shared"], function (sidebar, shared) {


    return {
        init: function () {

            $$("sidebar-context").attachTo($$("sidebar"));

            $$("sidebar-context").attachEvent("onItemClick", function (id) {
                switch (this.getItem(id).id) {
                    case 1:
                        var params = [];
                        $$("ticket-list").eachRow(
                            function (row) {
                                item = $$("ticket-list").getItem(row);
                                if (item.ticket_unread != "0")
                                    params.push(item.id);

                            }
                        )
                        var xhr = webix.ajax().sync().post('server/ticket-read.php', {ticket_id: params});
                        window.ticketFilter = {};
                        shared.loadTickets($$("sidebar").getSelectedItem().id);
                        shared.refreshTicketCount();

                        break;
                }

            });


            $$('sidebar').attachEvent("onBeforeSelect", function (id) {
                if (this.getItem(id).$count) {
                    // debugger;
                    return false;
                }
            });


            $$('sidebar').attachEvent("onAfterSelect", function (id) {

                    if ($$("sidebar").getSelectedItem().queue_type == 'reports') {
                        $$('formView').show();
                        return;
                    } else
                        $$('area-list').show();
                    window.ticketFilter = {};
                    shared.loadTickets(id);
                }
            );


            $$('sidebar').attachEvent("onAfterLoad", function () {

                shared.refreshTicketCount();

                var queueId = getParameterByName('queue');
                if (queueId != '') {
                    $$('sidebar').select(queueId);
                } else
                    $$('sidebar').select(-100);


            })


            $$('sidebar').attachEvent("onViewResize", function () {
                alert('ddd');
                //...
            })

            $$('sidebar').attachEvent("onItemDblClick", function (id, e, node) {
                if ($$("sidebar").getSelectedItem().ticket_unread_count == 0) return;
                var params = {};
                params['ticket_unread'] = ['>', '0']
                window.ticketFilter['ticket_unread'] = ['int', '>', '0'];
                shared.loadTickets(id);
            });

            $$('sidebar').attachEvent("onBeforeDrop", function (context, native_event) {
                if (context.target == "-100") {   //Queue All tickets
                    webix.alert({
                        title: "Ошибка",
                        text: "Сюда нельзя переместить!",
                        type: "alert-error"
                    });
                    return false;
                }


                var params = [];
                var tickets = [];
                for (var i = 0; i < context.source.length; i++) {
                    //   if ((context.from.getItem(context.source[i]).queue_id != context.target) && ($$('sidebar').getSelectedId()=="-200"))
                    sidebarItem = $$('sidebar').getSelectedItem();
                    if ((context.from.getItem(context.source[i]).user_id == 0) && (context.from.getItem(context.source[i]).queue_id == context.target))
                        continue;
                    tickets.push(parseInt(context.from.getItem(context.source[i]).ticket_id));
                }


                if (tickets.length > 0) {
                    nodeTarget = context.to.getItem(context.target);
                    var xhr = webix.ajax().sync().post("server/ticket-action.php", {
                        action: "move",
                        tickets: tickets,
                        id: context.target,
                        queue_type: nodeTarget.queue_type,
                        queue_name: nodeTarget.value.substr(0, nodeTarget.value.lastIndexOf('('))
                    });
                    dataRet = JSON.parse(xhr.responseText);
                    for (var i = 0; i < dataRet.length; i++) {
                        if (($$("sidebar").getSelectedId() == "-100") && ((context.target != "-1000") && (context.target != "-500") && (context.target != "-400"))) {
                            row = $$("ticket-list").getItem(dataRet[i].ticket_id);
                            row["ticket_queue"] = dataRet[i].ticket_queue;
                            row["ticket_user"] = dataRet[i].ticket_user;
                            row["queue_id"] = dataRet[i].queue_id;
                            row["ticket_status_name"] = dataRet[i].ticket_status_name;

                            $$("ticket-list").updateItem(dataRet[i].ticket_id, row);
                        }
                        else
                            $$("ticket-list").remove(dataRet[i].ticket_id);
                    }
                    shared.refreshTicketCount();
                }
                return false;
            });

            $$('sidebar').attachEvent("onBeforeDrop", function (context, native_event) {
                window.ticketsDisableUpdate = false;
            });


        }
    };


});