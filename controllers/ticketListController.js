define(["views/ticketList", "controllers/shared", "views/ticketDetail", "controllers/ticketDetailController"], function (ticketList, shared, ticketDetail, ticketDetailController) {

    function btnReplyClick(type) {
        ticketDetailController.editorShow(type);
    };


    function btnBlockUnBlockClick() {
        var xhr;
        if ($$('ticket-list').getSelectedItem().ticket_status_id == 'inprogress')
            xhr = webix.ajax().sync().post("server/ticket-action.php", {
                action: "unblock",
                ticket_id: $$('ticket-list').getSelectedItem().ticket_id
            });
        else
            xhr = webix.ajax().sync().post("server/ticket-action.php", {
                action: "block",
                ticket_id: $$('ticket-list').getSelectedItem().ticket_id
            });

        shared.refreshTicketAction(JSON.parse(xhr.responseText.trim()));
    }


    function btnOpenCloseClick() {
        tAction = "";
        if ($$('ticket-list').getSelectedItem().ticket_status_id == 'closed')
            tAction = "open";
        else
            tAction = "close";
        xhr = webix.ajax().sync().post("server/ticket-action.php", {
            action: tAction,
            ticket_id: $$('ticket-list').getSelectedItem().ticket_id
        });
        shared.refreshTicketAction(JSON.parse(xhr.responseText.trim()));
    }
    ;


    function getAttachmentLinks(data) {
        if (data == null)
            return "";

        retStr = "";
        for (var i = 0; i < data.length; i++) {
            retStr += '<a href="server/get-attachment.php?file=' + data[i].filePath + '">' + data[i].name + '</a>&nbsp;&nbsp;&nbsp;';
        }
        return retStr;
    }


    function refreshNew() {
        shared.loadNewTickets(window.ticketsLastUpdate);
    }


    return {
        refreshNew: refreshNew,
        init: function () {


            $$("ticket-list").attachEvent("onBeforeDrag", function (context, ev) {
                //shared.ticketUpdateDisable=true;
                window.ticketsDisableUpdate = true;
            });

            $$("ticket-list").attachEvent("onAfterDrop", function (context, native_event) {
                //shared.ticketUpdateDisable=false;
                window.ticketsDisableUpdate = false;
            });


            $$("ticket-list").attachEvent("onItemDblClick", function (id, e, node) {
                var newid = webix.uid();
                var view = $$('tabview').getMultiview();
                //view.addView({ id:newid, view: ticketDetail.$ui }, -1);


                var config = {
                    id: newid,
                    header: "dyn header",
                    body: ticketDetail.$ui
                    // body: {template : "ddd"}
                };

                $$('tabview').addView(config);


            });
            $$("ticket-list").attachEvent("onAfterLoad", function () {
            //    return;
                var ticketId = getParameterByName('ticket');

                if (gTicketId != 0) {
                    $$("ticket-list").select(gTicketId);
                    $$("ticket-list").showItem(gTicketId);
                    gTicketId = 0;
                    return;
                }

                if (ticketId != '')
                    $$("ticket-list").select(ticketId);
                else if ($$("ticket-list").count() > 0)
                    $$("ticket-list").select($$("ticket-list").getFirstId());

            });

            $$("ticket-list").attachEvent("onSelectChange", function () {
                //return;
                if ($$("ticket-list").getSelectedItem() == null)
                    return;

                if (Array.isArray($$("ticket-list").getSelectedItem()))
                    return;


                shared.enableDisableTicketButtons();


                $$("ticket-detail").clearAll();
                if ($$("ticket-list").getSelectedItem()) {
                    var xhr = webix.ajax().sync().get('models/ticketDetails.php?id=' + $$("ticket-list").getSelectedItem().ticket_id);
                    data = JSON.parse(xhr.responseText);
                    for (var i = 0; i < data.length; i++) {
                        //oData = JSON.parse(data[i]['ticket_message_attachments']);  
                        $$("ticket-detail").add({
                            user_id: data[i]['user_id'],
                            ticket_message_text: data[i]['ticket_message_text'],
                            ticket_message_date: data[i]['ticket_message_date'],
                            user_name: data[i]['user_name'],
                            //                         ticket_message_attachments: "<a href=\"server/get-attachment.php?file="+oData[0].filePath+"\">"+oData[0].name+"</a>",
                            ticket_message_attachments: getAttachmentLinks(JSON.parse(data[i]['ticket_message_attachments']))

                        });

                    }
                    //             $$("ticket-detail").load('server/get-details.php?id=' + $$("ticket-list").getSelectedItem().ticket_id); 
                }


                row = $$("ticket-list").getSelectedItem();
                if (row["ticket_unread"] != '0') {
                    var xhr = webix.ajax().sync().post('server/ticket-read.php', {ticket_id: JSON.stringify([row["ticket_id"]])});
                    row["$css"] = "ticket_row_normal";
                    row["ticket_unread"] = "1";
                    $$("ticket-list").updateItem(row.id, row);
                    shared.refreshTicketCount();
                }

            });


            $$('btn-ticket-block').attachEvent("onItemClick", function () {
                btnBlockUnBlockClick();
            });


            $$('btn-ticket-reply').attachEvent("onItemClick", function () {
                btnReplyClick();
            });

            $$('btn-ticket-close').attachEvent("onItemClick", function () {
                btnOpenCloseClick();
            });

            $$('btn-ticket-comment').attachEvent("onItemClick", function () {
                btnReplyClick(2);
            });


        }
    };
});