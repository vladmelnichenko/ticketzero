define(function () {

    return {
        init: function () {

            $$('ticket-area').attachEvent("onViewResize", function () {
                saveDesktop();
            });

            $$('tabview-tickets').attachEvent("onViewResize", function () {
                saveDesktop();
            });

            webix.event($$("tabview-resizer").$view, "onClick", function () {
            });

        }
    };
});