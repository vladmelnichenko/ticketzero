define(["views/ticketArea", "views/sidebar", "controllers/shared", "views/menus/autoanswerReply.js"], function (ticketArea, sidebar, shared, autoanswerReply) {


    function sendReply() {
        formData = $$('ticket-form-reply').getValues();
        editorText = CKEDITOR.instances["editor-msg"].getData();
        _editorHide();
        var xhr = webix.ajax().sync().post("server/ticket-action.php", {
            action: formData['mode'],
            ticket_id: $$('ticket-list').getSelectedItem().ticket_id,
            message: editorText,
            ticket_status_id: $$('ticket-list').getSelectedItem().ticket_status_id,
            closed_status: $$("cb-form-close-type").getValue(),
            reject_reason_id: $$("cb-form-close-reason").getValue(),
            message_type: $$("cb-form-reply-type").getValue()
        });
        var oDataRet = JSON.parse(xhr.responseText.trim());
        shared.refreshTicketAction(JSON.parse(xhr.responseText.trim()));
        $$("ticket-detail").add({
            user_id: gUserId,
            user_name: gUserName,
            ticket_message_text: oDataRet[0].ticket_message_text,
            ticket_message_date: new Date().toLocaleDateString() + ' ' + new Date().toLocaleTimeString(),
            ticket_message_attachments: ""
        }, 0);

    }


    function msgEditorKeyPress(event) {
        if (event.data.keyCode == CKEDITOR.CTRL + 13) {
            sendReply();
        }

    }

    function _editorShow(replyType) {
        $$('ticket-form-reply').setValues({mode: "reply"});
        $$("lt_close_reason").hide();
        $$("ticket-form-reply").show();
        $$('lt_message_type').show();
        if (replyType == null)
            $$('cb-form-reply-type').setValue(1);
        else
            $$('cb-form-reply-type').setValue(replyType);

        CKEDITOR.replace('editor-msg', {
            startupFocus: true,
            autoParagraph: false,
            enterMode: CKEDITOR.ENTER_BR,
            on: {'key': msgEditorKeyPress}
        });
        CKEDITOR.instances['editor-msg'].setData('');
        $$('btn-ticket-reply').disable();
        $$('btn-ticket-block').disable();
        $$('btn-ticket-close').disable();
        $$('btn-autoanswer-close').disable();
    }

    function _editorHide() {
        var e = CKEDITOR.instances['editor-msg'];
        e.setData('');
        e.destroy();
        e = null;
        $$("ticket-form-reply").hide();
        shared.enableDisableTicketButtons();
    }


    function setAutoAnswer(editor, id) {
        var xhr = webix.ajax().sync().get("server/get-autoanswer.php", {
            autoanswer_id: id,
            ticket_id: $$('ticket-list').getSelectedItem().ticket_id
        });
        var oDataRet = JSON.parse(xhr.responseText.trim());
        //editor.setData('');

        if (oDataRet['reject_reason_id'] == "0") {
            $$("cb-form-close-type").setValue(1);
            $$("cb-form-close-reason").setValue(0);
            $$("cb-form-close-reason").disable();
        }
        else {
            $$("cb-form-close-type").setValue(2);
            $$("cb-form-close-reason").setValue(oDataRet['reject_reason_id']);
        }

        editor.insertHtml(oDataRet['answer']);
        var ranges = editor.getSelection().getRanges();
        editor.insertHtml(oDataRet['sign']);
        editor.getSelection().selectRanges(ranges);
    }

    function _autoAnswer(target, id) {

        $$('ticket-form-reply').setValues({mode: target});


        if (!$$("ticket-form-reply").isVisible()) {
            $$("ticket-form-reply").show();


            if (target == "close") {
                $$("lt_close_reason").show();
                $$('lt_message_type').hide();
//                $$('btn-reply-form-reply').define("label", 'Открыть');
//                $$('btn-reply-form-reply').refresh();
            } else {
                $$("lt_close_reason").hide();
                $$('lt_message_type').show();
                $$('cb-form-reply-type').setValue(1);
            }


            if (CKEDITOR.instances['editor-msg'] == null) {
                CKEDITOR.replace('editor-msg', {
                    startupFocus: true, autoParagraph: false, enterMode: CKEDITOR.ENTER_BR, on: {
                        'key': msgEditorKeyPress,
                        'instanceReady': function (evt) {
                            $$('btn-ticket-reply').disable();
                            $$('btn-ticket-block').disable();
                            $$('btn-ticket-close').disable();
                            $$('btn-autoanswer-close').disable();
                            setAutoAnswer(evt.editor, id);
                        }
                    }
                });
            }
        } else {
            setAutoAnswer(CKEDITOR.instances['editor-msg'], id);
        }
    }


    return {
        autoAnswer: _autoAnswer,
        editorShow: _editorShow,
        editorHide: _editorHide,
        init: function () {


            $$('btn-reply-form-reply').attachEvent("onItemClick", function () {
                sendReply();
            });

            $$('btn-reply-form-close').attachEvent("onItemClick", function () {
                _editorHide();
            });


            $$("cb-form-close-type").attachEvent("onChange", function (newv, oldv) {
                if (newv == "1") {
                    $$("cb-form-close-reason").setValue(0);
                    $$("cb-form-close-reason").disable();
                } else
                    $$("cb-form-close-reason").enable();
            });

        }
    };
});