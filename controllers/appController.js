define(["controllers/shared"], function (shared) {

    function searchTicket() {
        var params = {};
        if ($$('ticket_search').getValue() == "") return;

        if (isNaN(parseInt($$('ticket_search').getValue())))
           params['ticket_subject'] = 'string:'+$$('ticket_search').getValue();
        else
           params['ticket_number'] = 'int:'+$$('ticket_search').getValue();

        params['search_closed'] = $$('ch_searchclosed').getValue().toString();
        window.ticketFilter = JSON.stringify(params);
        if ($$('sidebar').getSelectedId() == "-100")
            shared.loadTickets(-100);
        else
            $$('sidebar').select(-100);
        $$('searchPopup').hide();
    }


    webix.ui({
        view: "popup",
        id: "searchPopup",
        width: 300,
        body: {
            rows: [{
                view: "search",
                id: "ticket_search",
                on: {
                    onChange: function (newv, oldv) {
                        searchTicket();
                    }
                }
            }, {
                cols: [{
                    view: "checkbox",
                    label: 'Закрытые',
                    css: "cb_search",
                    id: 'ch_searchclosed',
                    on: {
                        onChange: function (newv, oldv) {
                            //    searchTicket();
                        }
                    }
                }
                    /*
                     ,

                     {view: "checkbox",
                     id: 'ch_iowner',
                     label: 'Я владелец',
                     on: {
                     onChange: function(newv, oldv) {
                     refreshTaskBoard();
                     }
                     }
                     }
                     */
                ]
            }, {
                borderless: !0,
                css: "extended_search",
                template: "<span>Extended search</span>",
                height: 40
            }]
        },
        on: {
            onShow: function () {
                $$('ticket_search').setValue('');
                $$('ticket_search').focus();
            }
        }
    });



    webix.ui({
        view: "submenu",
        id: "profilePopup",
        width: 200,
        padding: 0,
        data: [{
            id: 1,
            icon: "user",
            value: "Профайл"
        }, {
            $template: "Separator"
        }, {
            id: 4,
            icon: "sign-out",
            value: "Выход"
        }],
        type: {
            template: function(e) {

                return e.type ? "<div class='separator'></div>" : "<span class='webix_icon alerts fa-" + e.icon + "'></span><span>" + e.value + "</span>"
            }
        },
        on: {
            onMenuItemClick: function(id) {
                $$("profilePopup").hide();
                if (id == 3) {
                    $$("profilePopup").data.getItem("3").value = "calendar";
                    //alert("sddd");
                }
                ;
                if (id == 4) {

                    webix.confirm(
                        {
                            text: "Вы хотите выйти ?",
                            ok: "Да",
                            cancel: "нет",
                            callback: function(e)
                            {
                                if (e)
                                {
                                    var xhr = webix.ajax().sync().get("server/logout.php");
                                    document.cookie = 'tz_token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                                    window.location = window.location.href;
                                }
                            }
                        });


                }
                if (id == 1)
                    userSettings();
            }
        }
    });


    return {
        init: function () {


        }
    };
});