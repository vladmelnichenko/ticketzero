define(function () {


        function _arrayToQueryString(ina) {
            var out = new Array();

            for (key in ina) {
                out.push(key + '=' + encodeURIComponent(ina[key]));
            }

            return out.join('&');
        }
        ;

        Date.prototype.toMysqlFormat = function () {
            function pad(n) {
                return n < 10 ? '0' + n : n
            }

            return this.getFullYear() + "-" + pad(1 + this.getMonth()) + "-" + pad(this.getDate()) + " " + pad(this.getHours()) + ":" + pad(this.getMinutes()) + ":" + pad(this.getSeconds());
        };

        function _refreshTicketRecord(data) {
            for (var i = 0; i < data.length; i++) {
                row = $$("ticket-list").getItem(data[i].ticket_id);
                row["ticket_queue"] = data[i].ticket_queue;
                row["ticket_user"] = data[i].ticket_user;
                row["queue_id"] = data[i].queue_id;
                row["ticket_status_id"] = data[i].ticket_status_id;
                row["ticket_status_name"] = data[i].ticket_status_name;

                $$("ticket-list").updateItem(data[i].ticket_id, row);

                /*

                 if ($$("sidebar").getSelectedId() == "-100")  {
                 //sel = $$("ticket-list").getItem(dataRet[i].ticket_id);
                 row = $$("ticket-list").getItem(data[i].ticket_id);
                 row["ticket_queue"]=data[i].ticket_queue;
                 row["ticket_user"]=data[i].ticket_user;
                 row["queue_id"]=data[i].queue_id;
                 row["ticket_status_name"]=data[i].ticket_status_name;

                 $$("ticket-list").updateItem(data[i].ticket_id, row);
                 }
                 else
                 */
                //$$("ticket-list").remove(data[i].ticket_id);
            }
            _enableDisableTicketButtons();
        }
        ;


        function getTicketCount(dataCount, id, queue_type) {
            if (queue_type == 'my') {
                id = gUserId;
                queue_type = 'user';
            }

            if ((queue_type == 'inprogress') || (queue_type == 'archive') || (queue_type == 'trash'))
                id = 0;

            for (var i = 0; i < dataCount.length; i++)
                if ((dataCount[i].queue_id == id) && (dataCount[i].queue_type == queue_type))
                    return [dataCount[i].ticket_count, dataCount[i].ticket_unread_count];

            return [0, 0];

        }

        function _refreshTicketCount() {
            var xhr = webix.ajax().sync().get("server/ticket-count.php");
            ticketCount = JSON.parse(xhr.responseText);

            var node;
            var iTicketCount = 0;
            var iTicketUnreadCount = 0;

            $$('sidebar').data.each(
                function (node) {
                    if (node.$level == 2) {
                        newValues = getTicketCount(ticketCount, node.id, node.queue_type);
                        node.ticket_count = newValues[0];
                        node.ticket_unread_count = newValues[1];
                        // setTicketCount(node, newValue);
                        if ((node.queue_type != "archive") && (node.queue_type != "trash")) {
                            iTicketCount = iTicketCount + parseInt(newValues[0]);
                            iTicketUnreadCount = iTicketUnreadCount + parseInt(newValues[1]);

                        }
                    }
                }
            );

            node = $$('sidebar').getItem(-100);
            node.ticket_count = iTicketCount;
            node.ticket_unread_count = iTicketUnreadCount;
            $$('sidebar').refresh();
            if (iTicketUnreadCount > 0)
                document.title = 'TZ-[GRU] (' + iTicketUnreadCount + ')';
            else
                document.title = 'TZ-[GRU]';
        }
        ;


        function _enableDisableTicketButtons() {

            if (($$("ticket-list").getSelectedItem() == null) || ($$("sidebar").getSelectedItem().queue_type == 'trash')) {
                $$('btn-ticket-reply').disable();
                $$('btn-ticket-block').disable();
                $$('btn-autoanswer-close').disable();
                $$('btn-autoanswer-reply').disable();
                $$('btn-autoanswer-close').disable();
                $$('btn-ticket-close').define("label", 'Закрыть');
                $$('btn-ticket-close').refresh();
                $$('btn-ticket-close').disable();
                $$('btn-ticket-comment').disable();
                return;
            }


            if ($$("ticket-list").getSelectedItem().ticket_status_id == 'closed') {
                $$('btn-ticket-reply').disable();
                $$('btn-ticket-block').disable();
                $$('btn-autoanswer-close').disable();
                $$('btn-autoanswer-reply').disable();
                $$('btn-autoanswer-close').disable();
                $$('btn-ticket-close').define("label", 'Открыть');
                $$('btn-ticket-close').refresh();
                $$('btn-ticket-comment').disable();
                return;
            }


            $$('btn-ticket-close').define("label", 'Закрыть');
            $$('btn-ticket-close').refresh();


            if (($$("ticket-list").getSelectedItem().user_id != 0) && ($$("ticket-list").getSelectedItem().user_id != gUserId)) {
                $$('btn-ticket-reply').disable();
                $$('btn-ticket-block').disable();
                $$('btn-ticket-close').disable();
                $$('btn-autoanswer-reply').disable();
                $$('btn-autoanswer-close').disable();
                $$('btn-ticket-comment').disable();
            } else {
                $$('btn-ticket-reply').enable();
                $$('btn-ticket-block').enable();
                $$('btn-ticket-close').enable();
                $$('btn-autoanswer-reply').enable();
                $$('btn-autoanswer-close').enable();
                $$('btn-ticket-comment').enable();
            }

            if ((($$("ticket-list").getSelectedItem().ticket_status_id == 'new') || ($$("ticket-list").getSelectedItem().ticket_status_id == 'open'))
                && (($$("ticket-list").getSelectedItem().user_id == 0) || ($$("ticket-list").getSelectedItem().user_id == gUserId))) {
                //$$('btnTicketBlock').enable();
                $$('btn-ticket-block').define("label", 'БЛОКИРОВАТЬ');
                $$('btn-ticket-block').define("icon", 'lock');
                $$('btn-ticket-block').refresh();
            } else {

                $$('btn-ticket-block').define("label", 'РАЗБЛОКИРОВАТЬ');
                $$('btn-ticket-block').define("icon", 'unlock');
                $$('btn-ticket-block').refresh();
            }

        };


        function _refreshTicketAction(row) {

            //if (($$("sidebar").getSelectedItem().queue_type == "my") ||  ($$("sidebar").getSelectedItem().queue_type == "user")) {
            if (($$("sidebar").getSelectedId() != "-100") || (row[0].ticket_status_id == 'closed')) {
                selId = $$("ticket-list").getPrevId($$("ticket-list").getSelectedItem().id);
                $$("ticket-list").remove($$("ticket-list").getSelectedItem().ticket_id);
                if (selId == null)
                    selId = $$("ticket-list").getFirstId();
                if (selId != null)
                    $$("ticket-list").select(selId);
                _refreshTicketCount();
            }

            if ($$("sidebar").getSelectedId() == "-100") {
                _refreshTicketRecord(row);
                _refreshTicketCount();
            }
            ;


        }


        function _loadTickets(id) {


            var itemSidebar = $$('sidebar').getItem(id);


            $$("ticket-list").clearAll();
            $$("ticket-detail").clearAll();
            _enableDisableTicketButtons();

            urlTickets = "models/tickets.php?id=" + itemSidebar.id + "&queue_type=" + itemSidebar.queue_type;

            if (Object.keys(window.ticketFilter).length > 0)
                urlTickets += '&filter=' + window.ticketFilter;
            $$("ticket-list").load(urlTickets);
            //window.ticketFilter = "";

            //  document.title = 'TZ-[GRU]';
        }
        ;

        function _loadNewTickets(lastUpdate) {
            if (window.ticketsDisableUpdate)
                return;

            if (Object.keys(window.ticketFilter).length > 0) {
                _refreshTicketCount();
                return;
            }


            var sLastUpdate = '';

            if (lastUpdate != null) {
                window.ticketsLastUpdate = new Date();
                //var xhr = webix.ajax().sync().get("models/tickets.php?queue_type=all&last_update=" + lastUpdate.toMysqlFormat());
                var itemSidebar = $$('sidebar').getSelectedItem();
                var xhr = webix.ajax().sync().get("models/tickets.php?id=" + itemSidebar.ticket_id + "&queue_type=" + itemSidebar.queue_type);
                var data = JSON.parse(xhr.responseText);
                var cssUnread = 'ticket_row_normal';
                var itemSidebar = $$('sidebar').getSelectedItem();

                if (itemSidebar.queue_type == 'all' || itemSidebar.queue_type == 'queue')
                    for (var i = 0; i < data.length; i++) {
                        var res = $$("ticket-list").find(function (obj) {
                            return obj.id == data[i]['id'];
                        });
                        if (res.length == 0) {
                            if (data[i]['ticket_unread'] != '0')
                                cssUnread = 'ticket_row_new';
                            $$("ticket-list").add({
                                id: data[i]['id'],
                                $css: cssUnread,
                                ticket_id: data[i]['ticket_id'],
                                ticket_number: data[i]['ticket_number'],
                                ticket_status_id: data[i]['ticket_status_id'],
                                ticket_subject: data[i]['ticket_subject'],
                                ticket_queue: data[i]['ticket_queue'],
                                ticket_status_name: data[i]['ticket_status_name'],
                                ticket_user: data[i]['ticket_user'],
                                ticket_age: data[i]['ticket_age'],
                                ticket_sender: data[i]['ticket_sender'],
                                queue_id: data[i]['queue_id'],
                                ticket_status_id: data[i]['ticket_status_id'],
                                quene_type: data[i]['quene_type'],
                                ticket_unread: data[i]['ticket_unread'],
                                user_id: data[i]['user_id'],
                            }, 0);

                        } else {
                            row = $$("ticket-list").getItem(res[0].ticket_id);
                            if ((row["queue_id"] != data[i]['queue_id']) ||
                                (row["ticket_status_id"] != data[i]['ticket_status_id']) ||
                                (row["ticket_user"] != data[i]['ticket_user'])) {

                                row["ticket_queue"] = data[i]['ticket_queue'];
                                row["ticket_user"] = data[i]['ticket_user'];
                                row["queue_id"] = data[i]['queue_id'];
                                row["ticket_status_id"] = data[i]['ticket_status_id'];
                                row["ticket_status_name"] = data[i]['ticket_status_name'];
                                row["user_id"] = data[i]['user_id'];
                                $$("ticket-list").updateItem(res[0].ticket_id, row);
                            }

                        }
                    }


                $$("ticket-list").eachRow(
                    function (row) {
                        ticketFound = false;
                        for (var i = 0; i < data.length; i++) {
                            if (data[i]['ticket_id'] == row) {
                                ticketFound = true;
                                break;
                            }
                        }
                        if (!ticketFound)
                            $$("ticket-list").remove(row);

                    }
                )


                    /*                
                     for (var i = 0; i < data.length; i++)
                     {

                     if (data[i]['ticket_unread'] != '0')
                     cssUnread = 'ticket_row_new';
                     $$("ticket-list").add({
                     id: data[i]['id'],
                     $css: cssUnread,
                     ticket_id: data[i]['ticket_id'],
                     ticket_number: data[i]['ticket_number'],
                     ticket_status_id: data[i]['ticket_status_id'],
                     ticket_subject: data[i]['ticket_subject'],
                     ticket_queue: data[i]['ticket_queue'],
                     ticket_status_name: data[i]['ticket_status_name'],
                     ticket_user: data[i]['ticket_user'],
                     ticket_age: data[i]['ticket_age'],
                     ticket_sender: data[i]['ticket_sender'],
                     queue_id: data[i]['queue_id'],
                     ticket_status_id: data[i]['ticket_status_id'],
                     quene_type: data[i]['quene_type'],
                     ticket_unread: data[i]['ticket_unread']
                     }, 0);
                     }
                     */
                ;
                _refreshTicketCount();
            }
        }


        return {
            refreshTicketRecord: _refreshTicketRecord,
            refreshTicketCount: _refreshTicketCount,
            enableDisableTicketButtons: _enableDisableTicketButtons,
            refreshTicketAction: _refreshTicketAction,
            arrayToQueryString: _arrayToQueryString,
            loadTickets: _loadTickets,
            loadNewTickets: _loadNewTickets


        }
    }
);
