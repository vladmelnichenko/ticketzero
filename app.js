/*
 App configuration
 */

var gTicketId = 0;
var ticketsLastUpdate = new Date;
var ticketsDisableUpdate = false;


window.ticketFilter = {};

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};




function setCookie(name, value, expires, path, domain, secure) {
    document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}


function getCookie(name) {
    var cookie = " " + document.cookie;
    var search = " " + name + "=";
    var setStr = null;
    var offset = 0;
    var end = 0;
    if (cookie.length > 0) {
        offset = cookie.indexOf(search);
        if (offset != -1) {
            offset += search.length;
            end = cookie.indexOf(";", offset)
            if (end == -1) {
                end = cookie.length;
            }
            setStr = unescape(cookie.substring(offset, end));
        }
    }
    return (setStr);
}





function saveDesktop() {
    setCookie("sidebar", $$("sidebar").$width);
}


define([
    "views/toolbar",
    "views/sidebar",
    //"views/tabview",
    "views/ticketArea",
    "views/ticketList",
    "views/ticketDetail",
    "controllers/appController",
    "controllers/ticketListController",
    "controllers/sidebarController",
    "controllers/ticketAreaController",
    "controllers/ticketDetailController"
], function (toolbar, sidebar, ticketArea, ticketList, ticketDetail, appController, ticketListController, sidebarController, ticketAreaController, ticketDetailController) {


var lng = navigator.language.toLocaleLowerCase();

    if (lng.indexOf('-') !== -1)
        lng = lng.split('-')[0];


var oTr = JSON.parse(webix.ajax().sync().get("libs/ajax-lng/get-lng.php?name="+lng).responseText);

function tr(name) {
    if (oTr[name]!=undefined)
      return oTr[name];
    else
      return name+' - N/A';
}

    webix.ready(function () {
        webix.ui(
            {
                rows: [toolbar.$ui, {
                    cols: [sidebar.$ui,
                        {view: "resizer"},
                        ticketArea.$ui
                    ]
                }
                ]
            });

        var gSideBarController = sidebarController;

        ticketListController.init();
        sidebarController.init();
        ticketAreaController.init();
        ticketDetailController.init();

        var nWidth = getCookie("sidebar");
        if (nWidth == null)
            nWidth = 200;


        $$("sidebar").define("width", parseInt(nWidth) + 16);
        $$("sidebar").resize();

        document.title = 'TZ-[GRU]';
        setInterval(ticketListController.refreshNew, 15000);
    });


});