<?php

session_start();

error_reporting(E_ALL);

require_once('libs/template.class.php');
require_once('server/config/security.php');
require_once('server/config/safemysql.class.php');


class Login
{

    private $dataUser = false;
    private $workspaceId = false;
    private $urlParts = false;
    private $db = false;


    function __construct()
    {
        $this->urlParts = explode("/", $_SERVER['REQUEST_URI']);
        $this->db = new SafeMySQL();
    }


    private function login()
    {
        $dataUser = $this->db->getAll('SELECT user_id, user_password,user_salt, user_secondname, user_firstname, default_workspace_id FROM users WHERE user_email = ?s', $_POST['email']);

        $password = crypt($_POST['password'], $dataUser[0]['user_salt']);


        if ($password == $dataUser[0]['user_password']) {


            if (isset($_POST['rememberme'])) {

                if ($_POST['rememberme'] == 1) {
                    $login = $_POST['email'];
                    $token = crypt(time() . $login, salt());
                    setcookie('tz_token', $token, time() + 60 * 60 * 24 * 14, "/");
                    $sql = "UPDATE users SET user_token=?s WHERE user_id=?i";
                    $this->db->query($sql, $token, $dataUser[0]['user_id']);
                }
            }

//				session_start();
            $_SESSION['user_email'] = $_POST['email'];
            $_SESSION['user_name'] = $dataUser[0]['user_secondname'] . ' ' . $dataUser[0]['user_firstname'];
            $_SESSION['user_secondname'] = $dataUser[0]['user_secondname'];
            $_SESSION['user_firstname'] = $dataUser[0]['user_firstname'];
            $_SESSION['user_id'] = $dataUser[0]['user_id'];
            $_SESSION['default_workspace_id'] = $dataUser[0]['default_workspace_id'];
            $_SESSION['login'] = "true";
            echo "true";
        } else {
            echo "false";
        }
        exit;
    }


    private function setDataUserFromToken($token)
    {
        $this->dataUser = $this->db->getAll('SELECT * FROM users WHERE user_token = ?s', $token);
        return count($this->dataUser) > 0;
    }

    private function setDataUserFromSession()
    {
        $this->dataUser[0]['user_email'] = $_SESSION['user_email'];
        $this->dataUser[0]['user_firstname'] = $_SESSION['user_firstname'];
        $this->dataUser[0]['user_secondname'] = $_SESSION['user_secondname'];
        $this->dataUser[0]['user_id'] = $_SESSION['user_id'];
        $this->dataUser[0]['default_workspace_id'] = $_SESSION['default_workspace_id'];
        $_SESSION['login'] = 'false';
    }


    private function showLoginForm()
    {
        $tmpl = new Template('login');
        echo $tmpl->getText();
        exit;
    }

    /**
     *
     */
    public function doProcess()
    {



        if (isset($_POST['login_mode']) && ($_POST['login_mode'] == 'true'))
            $this->login();



        if (isset($_COOKIE['tz_token'])) {
            if (!$this->setDataUserFromToken($_COOKIE['tz_token']))
                $this->showLoginForm();

        }


        //print_r($_SESSION);

        if (isset($_SESSION['login']) && ($_SESSION['login'] == 'true')) {
            $this->setDataUserFromSession();
        }

        if (!$this->dataUser)
            $this->showLoginForm();


        // try find workspace
        $this->workspaceId = $this->db->getOne("SELECT workspace_id FROM workspaces WHERE workspace_name = ?s", end($this->urlParts));
        $dataWorkspace = false;
        if (!$this->workspaceId) {
            $this->workspaceId = $this->dataUser[0]['default_workspace_id'];
            if ($this->workspaceId != 0) {


                $dataWorkspace = $this->db->getAll("SELECT * FROM workspaces WHERE workspace_id = ?i", $this->workspaceId);
            } else {
                // TO-DO Show list workspaces
                exit;
            }

        }

        $_SESSION['user_email'] = $this->dataUser[0]['user_email'];
        $_SESSION['user_name'] = $this->dataUser[0]['user_secondname'] . ' ' . $this->dataUser[0]['user_firstname'];
        $_SESSION['user_id'] = $this->dataUser[0]['user_id'];
        $_SESSION["workspace_id"] = $dataWorkspace[0]['workspace_id'];
        $_SESSION["workspace_name"] = $dataWorkspace[0]['workspace_name'];

        $tmpl = new Template('app');
        $tmpl->setValueS('USER_ID', $_SESSION['user_id']);
        $tmpl->setValueS('USER_NAME', $_SESSION['user_name']);
        echo $tmpl->getText();
    }

}


$l = new Login();
$l->doProcess();


?>