<?php
session_start();

$aTicketId = json_decode($_POST['ticket_id']);

require_once('config/safemysql.class.php');
$db=new SafeMySQL();
$sql = "DELETE FROM ticket_unread WHERE ticket_id=?i AND user_id=?i";			

foreach($aTicketId as $ticketId)
   $db->query($sql,$ticketId, $_SESSION['user_id']);

$retData['result']="true";

header('Content-Type: application/json');
echo json_encode($retData);



?>