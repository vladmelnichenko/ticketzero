<?php
session_start();

$path = '../resources/images/avatars/';

isset($_GET['user_id']) ? $userId = $_GET['user_id'] : $userId = $_SESSION['user_id'];


if (file_exists($path . $userId . '.png'))
    $file = $path . $userId . '.png';
else
    $file = $path . '0.png';
header('Content-Type: image/png');
header('Content-Length: ' . filesize($file));
echo file_get_contents($file);
?>