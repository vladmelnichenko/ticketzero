<?php
session_start();
require_once('config/safemysql.class.php');
require_once('Event.class.php');


$tickets=json_decode($_POST['tickets']);
$Id=$_POST['id'];
$queueType=$_POST['queue_type'];


$db=new SafeMySQL();

$retData = array();


if (($queueType=="my") || ($queueType=="user") || ($queueType=="iprogress")) {
foreach ($tickets as $ticketId) {
  $sql = "UPDATE tickets SET user_id=?i WHERE ticket_id=?i";			
  
  
  if ($queueType=="iprogress")
      $sql = "UPDATE tickets SET user_id=?i, ticket_status_id='inprogress' WHERE ticket_id=?i";
  
  if ($Id=="-200") 
      $userId=$_SESSION['user_id'];
  else 
      $userId=$Id;
      
  echo $sql;
  $db->query($sql,$userId,$ticketId);
  array_push($retData,$ticketId);
  Event::send(900, $ticketId, 0, $_SESSION['user_id'], "", "");
}  

} else 
foreach ($tickets as $key => $value) {
  $sql = "UPDATE tickets SET queue_id=?i, user_id=0 WHERE ticket_id=?i";			
  $db->query($sql,$Id,$value);
  array_push($retData,$value);
  Event::send(800, $value, 0, $_SESSION['user_id'], "", "");
}  

$retData = $db->getAll("
    SELECT tik.*,IFNULL(CONCAT(usr.user_secondname,' ',SUBSTRING(usr.user_firstname,1,1)),usr.user_email) AS ticket_user  FROM
  (
SELECT
  t.*,
  ts.ticket_status_name,
  q.queue_name as ticket_queue
  FROM tickets t,
  ticket_status ts,
  queue q
WHERE t.ticket_status_id=ts.ticket_status_id
  AND t.queue_id=q.queue_id
  AND t.ticket_id IN (".implode(',',$retData).")
) tik LEFT JOIN users usr ON tik.user_id=usr.user_id
");



header('Content-Type: application/json');
echo json_encode($retData);


?>