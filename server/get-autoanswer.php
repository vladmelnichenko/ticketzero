<?php
session_start();
require_once('../server/config/safemysql.class.php');

$db = new SafeMySQL();

$dataAutoanswer = $db->getAll("
SELECT a.autoanswer_text,  a.reject_reason_id, rr.reject_reason_name
  FROM autoanswer a
  LEFT JOIN reject_reasons rr  ON a.reject_reason_id=rr.reject_reason_id
  WHERE a.autoanswer_id=?i
",$_GET['autoanswer_id']);

$dataTicket = $db->getAll("
SELECT
  t.*,
  IFNULL(CONCAT(u.user_secondname,' ',u.user_firstname),u.user_email) AS ticket_user_name,
  u.user_email as ticket_user_email
FROM (SELECT
  t.ticket_id,
  t.user_id,
  t.ticket_number,
  t.ticket_datecreate,
  t.ticket_subject,
  t.ticket_sender_email,
  t.ticket_sender_name,
  t.ticket_text,
  t.queue_id,
  t.ticket_status_id,
  q.queue_name
FROM 
     tickets t,
     ticket_event_type tep,
     queue q
WHERE t.queue_id=q.queue_id
AND t.ticket_id=?i
) t
  LEFT JOIN users u
    ON t.user_id = u.user_id
",$_GET['ticket_id']);


$templateAnswer=$dataAutoanswer[0]['autoanswer_text'];


$templateSign=$db->getOne("
    SELECT workspace_reply_sign
      FROM workspaces
     WHERE workspace_id=?i
     ",$_SESSION['workspace_id']);

        foreach ($dataTicket[0] as $key => $value) {
            $templateAnswer = str_replace('[' . strtoupper($key) . ']', $value, $templateAnswer);
            $templateSign = str_replace('[' . strtoupper($key) . ']', $value, $templateSign);
            $templateAnswer = str_replace('[EVENT_USER_NAME]', $_SESSION['user_name'], $templateAnswer);
            $templateSign = str_replace('[EVENT_USER_NAME]', $_SESSION['user_name'], $templateSign);
        }

$data['answer']=$templateAnswer;
$data['sign']=$templateSign;
$data['reject_reason_id']=$dataAutoanswer[0]['reject_reason_id'];
$data['reject_reason_name']=$dataAutoanswer[0]['reject_reason_name'];
header('Content-Type: application/json');
echo json_encode($data);

?>