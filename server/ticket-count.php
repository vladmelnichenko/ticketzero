<?php
session_start();
require_once('config/safemysql.class.php');
$db=new SafeMySQL();
$data = $db->getAll("SELECT qry.queue_id,  qry.queue_type, COUNT(qry.ticket_id) AS ticket_count, COUNT(tu.ticket_id) AS ticket_unread_count FROM (
SELECT q.queue_id, queue_type,t.ticket_id 
  FROM (
SELECT q.queue_id, 'queue' AS queue_type FROM
  queue q,
  group_queue gq,
  group_users ga
WHERE q.queue_id=gq.queue_id
AND ga.group_id=gq.group_id
AND ga.user_id=?i
  ) AS q LEFT JOIN tickets t ON q.queue_id=t.queue_id
  AND t.user_id=0
  AND t.ticket_status_id<>'closed' AND t.ticket_status_id<>'deleted'
  UNION ALL
 SELECT t.user_id, 'user' AS queue_type,  ticket_id
  FROM tickets t,
       group_users gu,
       group_users guu
  WHERE t.user_id=gu.user_id
    AND guu.group_id=gu.group_id
    AND guu.user_id=?i
    AND CASE WHEN t.user_id=1 AND t.ticket_status_id='inprogress' THEN t.user_id=-1 ELSE 1=1
    AND t.ticket_status_id<>'closed' AND t.ticket_status_id<>'deleted'
    END
  UNION ALL
 SELECT 0, 'inprogress' AS queue_type,  ticket_id
  FROM tickets t
  WHERE  t.user_id=?i
    AND  t.ticket_status_id='inprogress'
  UNION ALL
 SELECT 0, 'archive' AS queue_type,  ticket_id
  FROM tickets t
  WHERE  t.ticket_status_id='closed'
  UNION ALL
 SELECT 0, 'trash' AS queue_type,  ticket_id
  FROM tickets t
  WHERE  t.ticket_status_id='deleted'
  ) AS qry
  LEFT JOIN ticket_unread tu 
  ON qry.ticket_id=tu.ticket_id
  AND tu.user_id=?i
  GROUP BY qry.queue_id,  qry.queue_type
    
",$_SESSION['user_id'], $_SESSION['user_id'], $_SESSION['user_id'], $_SESSION['user_id']);

header('Content-Type: application/json');
echo json_encode($data);
?>