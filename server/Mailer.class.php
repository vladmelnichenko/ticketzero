<?php

session_start();
require_once '../libs/php-imap-master/src/PhpImap/IncomingMail.php';
require_once '../libs/php-imap-master/src/PhpImap/Mailbox.php';
require_once('config/safemysql.class.php');
require_once('Event.class.php');
require_once('../libs/PHPMailer-master/PHPMailerAutoload.php');
require_once '../libs/PHPMailer-master/class.phpmailer.php';
require_once '../libs/PHPMailer-master/class.smtp.php';

class Mailer
{

    private $mailbox;
    private $db;
    private $mailer;


    function __construct()
    {
   //     $this->mailbox = new PhpImap\Mailbox('{imap.gmail.com:993/ssl/novalidate-cert}', 'ur5vnp@gmail.com', 'airforcE2012', '../data/attachments');
        $this->mailbox = new PhpImap\Mailbox('{LINK.adventis.mc:143/novalidate-cert}', 'adventis.mc\support', '!UxmOWQ', '../data/attachments');
        $this->mailer = new PHPMailer();
        $this->db = new SafeMySQL();
    }

    private function newTicket($mail)
    {
        $queueId = 1;
        $data = $this->db->getAll("  
SELECT
  *
FROM mail_filters");
        foreach ($data as $row) {
            if ($row['mail_filter_sender_condition'] != '') {
                if (strpos($mail->fromAddress, $row['mail_filter_sender_condition'])) {
                    $queueId = $row['mail_filter_queue_id'];
                }
            }
        }


        $body = $mail->replaceInternalLinks("data/attachments");

        $sql = "INSERT INTO tickets(ticket_subject,ticket_sender_email, ticket_sender_name, ticket_text, queue_id, ticket_status_id, ticket_number)"
            . " VALUES(?s,?s,?s,?s,?i,?s,LPAD(nextval('sq_gru_ticketnumber'),8,'0'));";
        $this->db->query($sql, $mail->subject, $mail->fromAddress, $mail->fromName, $body, $queueId, 'new');
        $ticketId = $this->db->insertId();

        $attachments = array();
        foreach ($mail->attachments as $key => $value) {
            if (!strpos($key, '@')) {
                //echo $value->filePath;
                //$sstr='';
                //echo str_replace($value->filePath, str_replace('\\','/','/tz/data/attachments' . basename($value->filePath)), $value->filePath);
                $value->filePath = str_replace($value->filePath, str_replace('\\', '/', 'data/attachments/' . basename($value->filePath)), $value->filePath);
                array_push($attachments, $value);
            }
        }


        $sql = "INSERT INTO ticket_messages(ticket_id,ticket_message_sender_email, ticket_message_sender_name, ticket_message_text, ticket_message_attachments) VALUES(?i,?s,?s,?s,?s);";
        $this->db->query($sql, $ticketId, $mail->fromAddress, $mail->fromName, $body, json_encode($attachments));
        $ticketMessageId = $this->db->insertId();

        Event::send('new', $ticketId, $ticketMessageId, 0, $mail->fromAddress, $mail->fromName, $queueId);


        $data = $this->db->getAll("
SELECT
  u.user_id
FROM users u,
     group_users gu,
     group_queue gq
WHERE u.user_id = gu.user_id
AND gu.group_id = gq.group_id
AND gq.queue_id = ?i
", 1);
        foreach ($data as $row) {
            $sql = "INSERT INTO ticket_unread(ticket_id,user_id) VALUES(?i,?i);";
            $this->db->query($sql, $ticketId, $row['user_id']);
        }

        $data = $this->db->getAll("SELECT * FROM trace_filters WHERE trace_filter_active=1");
        $searchStr = mb_strtolower($mail->subject . ' ' . $body);

        foreach ($data as $row) {
            $keywords = explode(",", strtolower($row['trace_filter_keywords']));
            //print_r($keywords);
            foreach ($keywords as $word)
                /*               if (trim($word)=='принтер') {
                                 echo $word;
                                 echo '<br>'.$searchStr;
                                 $pos = mb_strpos($searchStr,trim($word));
                                 echo $pos;
                               }*/
                if (mb_strpos($searchStr, trim($word)) !== false) {
                    //echo 'YOOOO';
                    $sql = "INSERT INTO trace_tickets(trace_filter_id, ticket_id, queue_id) VALUES(?i,?i,?i);";
                    $this->db->query($sql, $row['trace_filter_id'], $ticketId, $queueId);
                    break;
                }
        }
    }

    private function newReply($mail)
    {
        $str = substr($mail->subject, strpos($mail->subject, "[") + 1, strpos($mail->subject, "]") - strpos($mail->subject, "[") - 1);
        $astr = explode(":", $str);
        $projectName = $astr[0];
        $aastr = explode("#", $astr[1]);
        $ticketNumber = $aastr[1];

        $ticketId = $this->db->getOne("  
         SELECT t.ticket_id
  FROM tickets  t,
       workspaces w,
       queue q
  WHERE t.queue_id=q.queue_id
    AND q.workspace_id=w.workspace_id
    AND w.workspace_name=?s
    AND t.ticket_number=?s", strtolower($projectName), $ticketNumber);

        echo strtolower($projectName);
        echo $ticketNumber;

        //print_r($ticketId);

        $body = $mail->replaceInternalLinks("data/attachments");

        $sql = "INSERT INTO ticket_messages(ticket_id,ticket_message_sender_email, ticket_message_sender_name, ticket_message_text) VALUES(?i,?s,?s,?s);";
        $this->db->query($sql, $ticketId, $mail->fromAddress, $mail->fromName, $body);
        $ticketMessageId = $this->db->insertId();
        Event::send('reply_client', $ticketId, $ticketMessageId, 0, $mail->fromAddress, $mail->fromName);
    }

    public function receiveMail()
    {

        $mails = array();
        $mailsIds = $this->mailbox->searchMailBox('ALL');

        if (!$mailsIds) {
            die('Mailbox is empty');
        }


        foreach ($mailsIds as $mailId) {

            $attachments = Array();

            $mail = $this->mailbox->getMail($mailId);

            $posTicket = stripos($mail->subject, 'Ticket#');
            if (!$posTicket)
                $this->newTicket($mail);
            else
                $this->newReply($mail);

        //    $this->mailbox->deleteMail($mailId);
        }
    }

    private function embed_images(&$body)
    {
        // get all img tags
        preg_match_all('/<img.*?>/', $body, $matches);

        if (!isset($matches[0]))
            return;
        // foreach tag, create the cid and embed image

        $i = 1;
        foreach ($matches[0] as $img) {
            // make cid
            $id = 'img' . ($i++);
            // replace image web path with local path

            preg_match('/src="(.*?)"/', $body, $m);

            if (!isset($m[1]))
                continue;
            $arr = parse_url($m[1]);

            if (!isset($arr['host']) && !isset($arr['path']))
                continue;
            // add
            //echo '../data/attachments/'.basename($arr['path']);
            //die;
            $this->mailer->AddEmbeddedImage('../data/attachments/' . basename($arr['path']), $id, 'attachment', 'base64', 'image/jpeg');
            $body = str_replace($img, '<img alt="" src="cid:' . $id . '" style="border: none;" />', $body);
            //   echo $body;
            // die;
        }
    }

    private function sendMail($mails)
    {

        $crendentials = array(
            'email' => 'ut4ukw@gmail.com', //Your GMail adress
            'password' => 'airforce2012'               //Your GMail password
        );


        $smtp = array(
            'host' => 'smtp.gmail.com',
            'port' => 587,
            'username' => 'ut4ukw@gmail.com',
            'password' => 'airforce12',
            'secure' => 'tls',
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        );


        //SMTP Configuration
        //   $mailer->isSMTP();
        $this->mailer->SMTPAuth = true; //We need to authenticate
        $this->mailer->IsSMTP(); // enable SMTP
        $this->mailer->SMTPDebug = 0;  // debugging: 1 = errors and messages, 2 = messages only
        $this->mailer->SMTPAuth = true;  // authentication enabled
        $this->mailer->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for GMail
        $this->mailer->Host = 'smtp.gmail.com';
        $this->mailer->Port = 587;
        $this->mailer->Username = 'ut4ukw@gmail.com';
        $this->mailer->Password = 'airforce2012';
        $this->mailer->SMTPKeepAlive = true;
        $this->mailer->isHTML(true);
        $this->mailer->CharSet = 'UTF-8';


        // $mailer->From = $crendentials['email'];
        $this->mailer->FromName = 'Служба поддержки ИТ'; //Optional

        $this->mailer->From = 'ur5vnp@gmail.com';
        // $mailer->Sender = 'ur5vnp@gmail.com';


        foreach ($mails as $mail) {

            $this->mailer->ClearAddresses();

// Send notification agents            
            if ($mail['send_user']) {
                foreach ($mail['recepient_users'] as $recepient)
                    $this->mailer->addAddress($recepient);
                $this->mailer->Subject = $mail['subject'];

                $bBody = $mail['body_user'];
                $this->embed_images($bBody);
                $this->mailer->MsgHTML($bBody);

                //echo $bBody;
                //exit;


                if (!$this->mailer->send()) {
                    echo 'Error sending mail : ' . $this->mailer->ErrorInfo;
                } else {
                    echo 'Message sent agent!';
                }
            }


// Send notification clients
            if ($mail['send_client']) {
                $this->mailer->ClearAddresses();
                $this->mailer->addAddress($mail['recepient_client']);
                $this->mailer->Subject = $mail['subject'];
                $this->mailer->MsgHTML($mail['body_client']);

                if (!$this->mailer->send()) {
                    echo 'Error sending mail : ' . $this->mailer->ErrorInfo;
                } else {
                    echo 'Message sent client!';
                }
            }
        }

        $this->mailer->SmtpClose();
    }

    private function getRecepients($queueId)
    {

        $data = $this->db->getAll("  
SELECT
  u.user_email
FROM users u,
     group_users gu,
     group_queue gq
WHERE u.user_id = gu.user_id
AND gu.group_id = gq.group_id
AND gq.queue_id = ?i
", $queueId);
        $ret = array();
        foreach ($data as $row) {
            array_push($ret, $row['user_email']);
        }
        return $ret;
    }

    private function normalizeTemplate($template, $ticketData)
    {
        foreach ($ticketData as $key => $value) {
            $template = str_replace('[' . strtoupper($key) . ']', $value, $template);
        }

        $template = str_replace('[TICKET_URL]', '<a href="http://localhost/tz/gru?queue=' . $ticketData['queue_id'] . '&ticket=' . $ticketData['ticket_id'] . '">', $template);         // !!! VLAD Change url 
        $template = str_replace('[/TICKET_URL]', '</a>', $template);         // !!! VLAD Change url 

        return $template;
    }

    private function getTemplate($eventTypeId, $ticketData)
    {

        $data = $this->db->getAll("  
SELECT ticket_notification_template_text_client,
       ticket_notification_template_text_user
  FROM
  ticket_notification_templates tnt,
  ticket_notification_workspace_templates tnwt
WHERE tnwt.ticket_notification_template_id= tnt.ticket_notification_template_id
  AND tnwt.workspace_id=?i
  AND tnwt.ticket_event_type_id=?s
  ", $_SESSION['workspace_id'], $eventTypeId);
        //  echo $_SESSION['workspace_id'];
        //print_r($data);


        $ret = array();
        $ret['ticket_notification_template_text_client'] = $this->normalizeTemplate($data[0]['ticket_notification_template_text_client'], $ticketData);
        $ret['ticket_notification_template_text_user'] = $this->normalizeTemplate($data[0]['ticket_notification_template_text_user'], $ticketData);
        return $ret;
    }

    /*
      private function getSubject($ticketNumber,$ticketSubject) {
      $ret='[TZ:Ticket#'.$ticketSubject.']'
      }
     * 
     */

    public function sendNotifications()
    {
        $data = $this->db->getAll("  
SELECT
  te.*,
  tm.ticket_message_date,
  tm.ticket_message_text,
  IFNULL(CONCAT(u.user_secondname,' ',u.user_firstname),u.user_email) AS event_user_name,
  IFNULL(CONCAT(u1.user_secondname,' ',u1.user_firstname),u1.user_email) AS ticket_user_name,
  u1.user_email as ticket_user_email
FROM (SELECT
  t.ticket_id,
  t.ticket_number,
  t.ticket_datecreate,
  t.ticket_subject,
  t.ticket_sender_email,
  t.ticket_sender_name,
  t.ticket_text,
  t.queue_id,
  t.ticket_status_id,
  q.queue_name,
  te.ticket_event_date,
  te.ticket_message_id,
  tep.ticket_event_type_id,
  tep.ticket_event_type_name,
  te.user_id AS event_user_id,
  t.user_id AS ticket_user_id,
  w.workspace_notify_sign
FROM ticket_events te,
     tickets t,
     ticket_event_type tep,
     queue q,
  workspaces w
WHERE t.ticket_id = te.ticket_id
AND tep.ticket_event_type_id = te.ticket_event_type_id
AND t.queue_id=q.queue_id
AND q.workspace_id=w.workspace_id
AND te.ticket_event_processed = 0) te
  LEFT JOIN users u
    ON te.event_user_id = u.user_id
  LEFT JOIN users u1
    ON te.ticket_user_id = u1.user_id
  LEFT JOIN ticket_messages tm
    ON te.ticket_message_id=tm.ticket_message_id
ORDER BY te.ticket_event_date
");

        $mails = array();






        foreach ($data as $row) {

            $aMail = array();


            $aMail['recepient_client'] = $row['ticket_sender_email'];
            $templates = $this->getTemplate($row['ticket_event_type_id'], $row);
            $aMail['body_user'] = $templates['ticket_notification_template_text_user'];
            $aMail['body_client'] = $templates['ticket_notification_template_text_client']."<br>".$row['workspace_notify_sign'];
           // echo $aMail['body_client'];
            $aMail['subject'] = '[' . strtoupper($_SESSION['workspace_name']) . ':Ticket#' . $row['ticket_number'] . '] ' . $row['ticket_subject'];

            //echo $mailUser['body_client'];
            $aMail['send_user'] = true;
            $aMail['send_client'] = true;

            //reply agent,  block, unblock, accept, reject
            if (($row['ticket_event_type_id'] == 'unblock') ||
             //   ($row['ticket_event_type_id'] == 'internal_message') ||
                ($row['ticket_event_type_id'] == 'block') ||
                ($row['ticket_event_type_id'] == 'reply_agent') ||
                ($row['ticket_event_type_id'] == 'accept') ||
                ($row['ticket_event_type_id'] == 'reject')
            )
                $aMail['send_user'] = false;

            //reply client
            if (($row['ticket_event_type_id'] == 'reply_client') || ($row['ticket_event_type_id'] == 'internal_message')) {
                $aMail['send_client'] = false;
            }

            // assign agent
            if (($row['ticket_event_type_id'] == 'assign') || ($row['ticket_event_type_id'] == 'reply_agent'))
                $aMail['recepient_users'] = array($row['ticket_user_email']);  // assign to agent
            else
                $aMail['recepient_users'] = $this->getRecepients($row['queue_id']);


            array_push($mails, $aMail);


            /*
              $arr = array();
              $arr['car'] = 'Чайка';
             * 
             */
        }


//print_r($mails);
        $this->sendMail($mails);
    }

}

/*


  //$mailId = reset($mailsIds);
  echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />";
  foreach ($mailsIds as $mailId) {
  $mail = $mailbox->getMail($mailId);
  echo $mail->subject."<br>";
  echo $mail->fromName."<br>";
  echo $mail->fromAddress."<br>";
  //echo $mail->replaceInternalLinks("")."<br>";

  echo "<br>";

  $body = $mail->replaceInternalLinks("/tz/data/attachments");



  }
  } */

$p = new Mailer();
$p->receiveMail();
//$p->sendNotifications();
?>