<?php
session_start();
session_unset();
session_destroy();

if (isset($_COOKIE['tz_token'])) {
    unset($_COOKIE['tz_token']);
    setcookie('tz_token', '', time() - 3600, '/'); // empty value and old timestamp
}
?>