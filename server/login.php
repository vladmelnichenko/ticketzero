<?php
require_once('config/security.php');
require_once('config/safemysql.class.php');

if(isset($_POST['email']) && isset($_POST['password']))
{
    $db=new SafeMySQL();	
    $dataUser = $db->getAll('SELECT user_id, user_password,user_salt, user_secondname, user_firstname, default_workspace_id FROM users WHERE user_email = ?s',$_POST['email']);

    $password=crypt($_POST['password'], $dataUser[0]['user_salt']);


    if ($password == $dataUser[0]['user_password']) {


	if(isset($_POST['rememberme']))
	{

		if($_POST['rememberme'] == 1)
		{
			$login = $_POST['email'];
			$token  = crypt(time().$login, salt());
			setcookie('tz_token', $token, time() + 60 * 60 * 24 * 14, "/");
		    $sql = "UPDATE users SET user_token=?s WHERE user_id=?i";
		    $db->query($sql,$token,$dataUser[0]['user_id']);
		}
	}

	 session_start();
	 $_SESSION['user_email']=$_POST['email'];
	 $_SESSION['user_name']=$dataUser[0]['user_secondname'].' '.$dataUser[0]['user_firstname'];
     $_SESSION['user_secondname']=$dataUser[0]['user_secondname'];
     $_SESSION['user_firstname']=$dataUser[0]['user_firstname'];
	 $_SESSION['user_id']=$dataUser[0]['user_id'];
	 $_SESSION['default_workspace_id']=$dataUser[0]['default_workspace_id'];
	 $_SESSION['login']="true";
	 echo "true";
	 } else {
	   echo "false"; 	
	 }
}


?>