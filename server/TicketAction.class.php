<?php

session_start();
require_once('config/safemysql.class.php');
require_once('Event.class.php');

class TicketAction {

    private $db;

    function __construct() {
        $this->db = new SafeMySQL();
    }

    private function getTicketData($aTicketId) {
        return $this->db->getAll("
SELECT
  tik.*,
  IFNULL(CONCAT(usr.user_secondname, ' ', SUBSTRING(usr.user_firstname, 1, 1)), usr.user_email) AS ticket_user
FROM (SELECT
  t.*,
  ts.ticket_status_name,
  q.queue_name AS ticket_queue
FROM tickets t,
     ticket_status ts,
     queue q
WHERE t.ticket_status_id = ts.ticket_status_id
AND t.queue_id = q.queue_id
AND t.ticket_id IN (" . implode(',', $aTicketId) . ")) tik
  LEFT JOIN users usr
    ON tik.user_id = usr.user_id
    ");
    }

    private function reply($params) {

        $sql = "INSERT INTO ticket_messages(ticket_id,ticket_message_text, user_id, ticket_message_type) VALUES(?i,?s,?i, ?i);";
        $this->db->query($sql, $params['ticket_id'], $params['message'], $_SESSION['user_id'], $params['message_type']);
        $messageId = $this->db->insertId();

        if ($params['message_type']=='1') {
          $sql = "UPDATE tickets SET user_id=?i, ticket_status_id='inprogress' WHERE ticket_id=?i";
          $this->db->query($sql, $_SESSION['user_id'], $params['ticket_id']);
          Event::send('reply_agent', $params['ticket_id'], $messageId, $_SESSION['user_id'], "", "");
        } else {
          Event::send('internal_message', $params['ticket_id'], $messageId, $_SESSION['user_id'], "", "");
        }
        

        $retData = $this->getTicketData(array($params['ticket_id']));
        $retData[0]['ticket_message_text'] = $params['message'];

        return $retData;
    }

    private function move($params) {
        $aTickets = json_decode($params['tickets']);
        $Id = $params['id'];
        $aTicketId = array();
        if (($_POST['queue_type'] == "my") || ($params['queue_type'] == "user") || ($params['queue_type'] == "inprogress")) {
            foreach ($aTickets as $ticketId) {
                $sql = "UPDATE tickets SET user_id=?i, ticket_status_id='open' WHERE ticket_id=?i";

                if ($params['queue_type'] == "inprogress")
                    $sql = "UPDATE tickets SET user_id=?i, ticket_status_id='inprogress' WHERE ticket_id=?i";

                if (($_POST['id'] == "-200") || ($_POST['id'] == "-300"))
                    $userId = $_SESSION['user_id'];
                else
                    $userId = $Id;

                $this->db->query($sql, $userId, $ticketId);
                array_push($aTicketId, $ticketId);
                if ($params['queue_type'] == "inprogress")
                    Event::send('open', $ticketId, 0, $_SESSION['user_id'], "", "");
                else
                    Event::send('assign', $ticketId, 0, $_SESSION['user_id'], "", "");
            }
        } elseif ($_POST['queue_type'] == "archive") {
            foreach ($aTickets as $key => $value) {
                $sql = "UPDATE tickets SET ticket_status_id='closed' WHERE ticket_id=?i";
                $this->db->query($sql, $value);
                array_push($aTicketId, $value);
//                Event::send(8, $value, 0, $_SESSION['user_id'], "", "");
            }
        } elseif ($_POST['queue_type'] == "trash") {
            foreach ($aTickets as $key => $value) {
                $sql = "UPDATE tickets SET ticket_status_id='deleted' WHERE ticket_id=?i";
                $this->db->query($sql, $value);
                array_push($aTicketId, $value);
//                Event::send(8, $value, 0, $_SESSION['user_id'], "", "");
            }
        } else {
            foreach ($aTickets as $key => $value) {
                $sql = "UPDATE tickets SET queue_id=?i, user_id=0, ticket_status_id='new' WHERE ticket_id=?i";
                $this->db->query($sql, $Id, $value);
                array_push($aTicketId, $value);
                Event::send('move', $value, 0, $_SESSION['user_id'], "", "");
            }
        }
        return $this->getTicketData($aTickets);
    }

    private function block($params) {

        $sql = "UPDATE tickets SET user_id=?i, ticket_status_id='inprogress' WHERE ticket_id=?i";
        $this->db->query($sql, $_SESSION['user_id'], $params['ticket_id']);


        Event::send('block', $params['ticket_id'], 0, $_SESSION['user_id'], "", "");

        $retData = $this->getTicketData(array($params['ticket_id']));

        return $retData;
    }

    private function unblock($params) {

        $sql = "UPDATE tickets SET user_id=0, ticket_status_id='open' WHERE ticket_id=?i";
        $this->db->query($sql, $params['ticket_id']);



        Event::send('unblock', $params['ticket_id'], 0, $_SESSION['user_id'], "", "");

        $retData = $this->getTicketData(array($params['ticket_id']));

        return $retData;
    }

    private function close($params) {

        $rejectReasonId = 0;
        $closedStatus = 0;
        if (isset($params['closed_status']))
            $closedStatus = $params['closed_status'];
        else
            $closedStatus = 1;
        if ($closedStatus == 2)
            $rejectReasonId = $params['reject_reason_id'];

        
        $messageId = 0;
            if (isset($params['message'])) {
                $sql = "INSERT INTO ticket_messages(ticket_id,ticket_message_text, user_id) VALUES(?i,?s,?i);";
                $this->db->query($sql, $params['ticket_id'], $params['message'], $_SESSION['user_id']);
                $messageId = $this->db->insertId();
            }
        
        
        $sql = "UPDATE tickets SET ticket_status_id='closed', closed_status=?i, reject_reason_id=?i, ticket_dateclose=now() WHERE ticket_id=?i";
        $this->db->query($sql, $closedStatus, $rejectReasonId, $params['ticket_id']);

        if ($closedStatus == 1) {
            Event::send('accept', $params['ticket_id'], $messageId, $_SESSION['user_id'], "", "");
        } else
            Event::send('reject', $params['ticket_id'], $messageId, $_SESSION['user_id'], "", "");

        $retData = $this->getTicketData(array($params['ticket_id']));

        return $retData;
    }

    private function open($params) {


        $sql = "UPDATE tickets SET ticket_status_id=2 WHERE ticket_id=?i";
        $this->db->query($sql, $params['ticket_id']);


        //Event::send(3, $params['ticket_id'], 0, $_SESSION['user_id'], "", "");

        $retData = $this->getTicketData(array($params['ticket_id']));

        return $retData;
    }

    public function runAction($actionName, $params) {
        $retData = $this->$actionName($params);

        header('Content-Type: application/json');
        echo json_encode($retData);
    }

}

?>